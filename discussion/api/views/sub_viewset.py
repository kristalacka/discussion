from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from api.models import Sub
from api.serializers import SubSerializer
from api.permissions import JwtAuthentication

from api.permissions import IsCreatorAdminOrReadOnly


class SubViewSet(viewsets.ModelViewSet):
    authentication_classes = [JwtAuthentication]
    permission_classes = [IsCreatorAdminOrReadOnly]

    queryset = Sub.objects.all()
    serializer_class = SubSerializer
    lookup_field = 'title'

    @action(detail=True,
            methods=['post'],
            permission_classes=[IsAuthenticated])
    def subscribe(self, request, pk=None, **kwargs):
        sub = self.get_object()
        sub.members.add(request.user)

        return Response(status=status.HTTP_200_OK)

    @action(detail=True,
            methods=['post'],
            permission_classes=[IsAuthenticated])
    def unsubscribe(self, request, pk=None, **kwargs):
        sub = self.get_object()
        sub.members.remove(request.user)

        return Response(status=status.HTTP_200_OK)

    def get_serializer_context(self):
        context = super(SubViewSet, self).get_serializer_context()
        context.update({"request": self.request})
        return context