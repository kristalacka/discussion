from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status

from api.permissions import JwtAuthentication
from api.serivces.jwt_service import JwtService


@api_view(['POST'])
def login(request):
    return JwtService().login(request)


@api_view(['POST'])
def refresh(request):
    return JwtService().refresh(request)


@api_view(['POST'])
@authentication_classes((JwtAuthentication, ))
@permission_classes((IsAuthenticated, ))
def logout(request):
    JwtService().logout(request)
    return Response(status=status.HTTP_200_OK)
