from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import NotFound
from rest_framework.permissions import IsAuthenticated
from api.models import Comment, CommentVote
from api.models.post import Post
from api.serializers import CommentSerializer, VoteSerializer
from api.permissions import JwtAuthentication
from rest_framework.response import Response
from rest_framework import status

from api.permissions import IsCreatorAdminOrReadOnly


class CommentViewSet(viewsets.ModelViewSet):
    authentication_classes = [JwtAuthentication]
    permission_classes = [IsCreatorAdminOrReadOnly]

    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

    def get_queryset(self):
        if sub_pk := self.kwargs.get('sub_title'):
            if post_pk := self.kwargs.get('post_pk'):
                try:
                    post = Post.objects.get(pk=post_pk)
                except Post.DoesNotExist:
                    raise NotFound("Post does not exist")
                if post.sub.title != sub_pk:
                    raise NotFound("Post doesn't belong to sub")

                return self.queryset.filter(post=post_pk, parent=None)

        raise NotFound("Comment not found")

    def retrieve(self, request, *args, **kwargs):
        instance = Comment.objects.get(pk=kwargs['pk'])
        if instance.post.id != int(
                kwargs['post_pk']
        ) or instance.post.sub.title != kwargs['sub_title']:
            return Response(status=404)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        instance = Comment.objects.get(pk=kwargs['pk'])
        if instance.post.id != int(
                kwargs['post_pk']
        ) or instance.post.sub.title != kwargs['sub_title']:
            return Response(status=404)
        serializer = self.get_serializer(instance,
                                         data=request.data,
                                         partial=False)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        try:
            obj = super().create(request, *args, **kwargs)
            comment = Comment.objects.get(pk=obj.data['id'])
            vote = CommentVote(user=request.user, comment=comment, vote=1)
            vote.save()
            obj.data['score'] = 1
            obj.data['vote'] = 1
            return obj
        except:
            return Response({'error': 'Post not found'},
                            status=status.HTTP_400_BAD_REQUEST)

    def get_object(self):
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field

        assert lookup_url_kwarg in self.kwargs, (
            'Expected view %s to be called with a URL keyword argument '
            'named "%s". Fix your URL conf, or set the `.lookup_field` '
            'attribute on the view correctly.' %
            (self.__class__.__name__, lookup_url_kwarg))

        # obj = get_object_or_404(queryset, **filter_kwargs)
        obj = Comment.objects.get(pk=self.kwargs['pk'])

        # May raise a permission denied
        self.check_object_permissions(self.request, obj)

        return obj

    @action(detail=True,
            methods=['post'],
            permission_classes=[IsAuthenticated])
    def vote(self, request, pk=None, **kwargs):
        serializer = VoteSerializer(data=request.data)
        if serializer.is_valid():
            try:
                comment_vote = CommentVote.objects.get(
                    user=request.user, comment=self.get_object())
                comment_vote.vote = serializer.validated_data['vote']
                comment_vote.save()
            except CommentVote.DoesNotExist:
                comment_vote = CommentVote(
                    user=request.user,
                    comment=self.get_object(),
                    vote=serializer.validated_data['vote'])
                comment_vote.save()
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

        return Response(status=status.HTTP_200_OK)

    def get_serializer_context(self):
        context = super(CommentViewSet, self).get_serializer_context()
        return context