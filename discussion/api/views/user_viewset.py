from rest_framework import mixins, viewsets
from django.contrib.auth.models import User
from api.serializers import UserSerializer
from api.permissions import JwtAuthentication

from api.permissions import IsSelf
from api.serializers import UserBasicSerializer


class UserViewSet(mixins.RetrieveModelMixin, mixins.ListModelMixin,
                  mixins.DestroyModelMixin, mixins.UpdateModelMixin,
                  viewsets.GenericViewSet):
    authentication_classes = [JwtAuthentication]
    permission_classes = [IsSelf]

    queryset = User.objects.all()

    def get_serializer_class(self):
        if self.request.user.is_staff:
            return UserSerializer
        return UserBasicSerializer
