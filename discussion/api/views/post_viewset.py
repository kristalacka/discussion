from django.db.models.aggregates import Sum
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from api.models import Post, Sub
from api.models.votes import PostVote
from api.serializers import PostSerializer
from api.permissions import JwtAuthentication
from api.serializers import VoteSerializer
from rest_framework.response import Response

from api.permissions import IsCreatorAdminOrReadOnly
import datetime


class PostViewSet(viewsets.ModelViewSet):
    authentication_classes = [JwtAuthentication]
    permission_classes = [IsCreatorAdminOrReadOnly]

    queryset = Post.objects.all()
    serializer_class = PostSerializer

    def get_queryset(self):
        if sub_pk := self.kwargs.get('sub_title'):
            return self.queryset.filter(sub__title=sub_pk)
        else:
            return self.queryset.filter(
                date_created__gte=datetime.datetime.now() -
                datetime.timedelta(days=1)).annotate(
                    score=Sum('postvote__vote')).order_by('-score')

    def create(self, request, *args, **kwargs):
        try:
            obj = super().create(request, *args, **kwargs)
            post = Post.objects.get(pk=obj.data['id'])
            vote = PostVote(user=request.user, post=post, vote=1)
            vote.save()
            obj.data['score'] = 1
            obj.data['vote'] = 1
            return obj
        except Exception as e:
            return Response({'error': 'Sub not found'},
                            status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True,
            methods=['post'],
            permission_classes=[IsAuthenticated])
    def vote(self, request, pk=None, **kwargs):
        serializer = VoteSerializer(data=request.data)
        if serializer.is_valid():
            try:
                post_vote = PostVote.objects.get(user=request.user,
                                                 post=self.get_object())
                post_vote.vote = serializer.validated_data['vote']
                post_vote.save()
            except PostVote.DoesNotExist:
                post_vote = PostVote(user=request.user,
                                     post=self.get_object(),
                                     vote=serializer.validated_data['vote'])
                post_vote.save()
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

        return Response(status=status.HTTP_200_OK)