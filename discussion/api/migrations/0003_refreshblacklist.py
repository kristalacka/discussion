# Generated by Django 3.2.3 on 2021-11-14 16:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_tokenblacklist'),
    ]

    operations = [
        migrations.CreateModel(
            name='RefreshBlacklist',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('token', models.BinaryField(max_length=512)),
            ],
        ),
    ]
