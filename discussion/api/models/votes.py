from django.db import models
from django.contrib.auth.models import User
from .post import Post
from .comment import Comment


class Vote(models.IntegerChoices):
    UPVOTE = 1
    DOWNVOTE = -1
    NONE = 0


class PostVote(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    vote = models.IntegerField(choices=Vote.choices)


class CommentVote(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    comment = models.ForeignKey(Comment, on_delete=models.CASCADE)
    vote = models.IntegerField(choices=Vote.choices)
