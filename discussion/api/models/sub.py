from django.db import models
from django.contrib.auth.models import User


class Sub(models.Model):
    creator = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=100, unique=True)
    description = models.TextField()
    date_created = models.DateTimeField(auto_now_add=True)
    members = models.ManyToManyField(User, related_name='members')