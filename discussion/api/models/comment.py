from django.db import models
from django.contrib.auth.models import User
from .post import Post


class Comment(models.Model):
    creator = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    parent = models.ForeignKey('self',
                               on_delete=models.SET_NULL,
                               blank=True,
                               null=True)
    text = models.TextField(null=True)
    date_created = models.DateTimeField(auto_now_add=True)
    last_edit = models.DateTimeField(default=None, blank=True, null=True)
