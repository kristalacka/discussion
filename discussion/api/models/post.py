from django.db import models
from django.contrib.auth.models import User
from .sub import Sub


class Post(models.Model):
    creator = models.ForeignKey(User, on_delete=models.CASCADE)
    sub = models.ForeignKey(Sub, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    text = models.TextField()
    date_created = models.DateTimeField(auto_now_add=True)
    last_edit = models.DateTimeField(default=None, blank=True, null=True)