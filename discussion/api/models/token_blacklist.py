from django.db import models


class TokenBlacklist(models.Model):
    token = models.BinaryField(max_length=512)


class RefreshBlacklist(models.Model):
    token = models.BinaryField(max_length=512)