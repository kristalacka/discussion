import time
from django.contrib.auth.models import User
from django.http.response import ResponseHeaders
import jwt
from rest_framework import status
from rest_framework.authentication import get_authorization_header

from api.models.token_blacklist import TokenBlacklist, RefreshBlacklist
from rest_framework.response import Response
from django.contrib.auth import authenticate


class JwtService():
    JWT_EXPIRY_MINUTES = 10
    REFRESH_EXPIRY_MINUTES = 60

    @staticmethod
    def login(request):
        username = request.data.get('username', None)
        password = request.data.get('password', None)
        if username is None or password is None:
            return Response(
                {'error': 'username or password fields not provided'},
                status=status.HTTP_400_BAD_REQUEST)

        user = authenticate(username=username, password=password)
        if user is None:
            return Response({'error': 'invalid user credentials'},
                            status=status.HTTP_400_BAD_REQUEST)

        payload = {
            'username': user.get_username(),
            'id': user.id,
            'iat': time.time(),
            'exp': time.time() + JwtService.JWT_EXPIRY_MINUTES * 60
        }

        refresh_payload = {
            'username': user.get_username(),
            'id': user.id,
            'iat': time.time(),
            'exp': time.time() + JwtService.REFRESH_EXPIRY_MINUTES * 60
        }

        response = {
            'token': jwt.encode(payload, "SECRET_KEY"),
            'refresh': jwt.encode(refresh_payload, "REFRESH_KEY"),
            'is_admin': user.is_superuser,
            'user_id': user.id
        }
        return Response(response, status=status.HTTP_200_OK)

    @staticmethod
    def logout(request):
        token = get_authorization_header(request).split()[1]
        TokenBlacklist.objects.create(token=token)
        refresh_token = request.data.get('refresh', None)
        if refresh_token is None:
            return Response({'msg': 'refresh token required'})

        RefreshBlacklist.objects.create(token=refresh_token.encode())
        return Response()

    @staticmethod
    def refresh(request):
        refresh_token = request.data.get('refresh', None)
        if refresh_token is None:
            return Response({'error': 'refresh token not provided'},
                            status=status.HTTP_400_BAD_REQUEST)

        try:
            data = jwt.decode(refresh_token,
                              "REFRESH_KEY",
                              algorithms=['HS256'])
        except (jwt.exceptions.ExpiredSignatureError,
                jwt.exceptions.InvalidSignatureError):
            return Response({'error': 'refresh token has expired'},
                            status=status.HTTP_400_BAD_REQUEST)

        try:
            user = User.objects.get(username=data['username'], id=data['id'])
        except User.DoesNotExist:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        if RefreshBlacklist.objects.filter(
                token=refresh_token.encode()).exists():
            return Response({'msg': 'token invalidated by logout'},
                            status=status.HTTP_400_BAD_REQUEST)

        payload = {
            'username': user.get_username(),
            'id': user.id,
            'iat': time.time(),
            'exp': time.time() + JwtService.JWT_EXPIRY_MINUTES * 60
        }

        refresh_payload = {
            'username': user.get_username(),
            'id': user.id,
            'iat': time.time(),
            'exp': time.time() + JwtService.REFRESH_EXPIRY_MINUTES * 60
        }
        response = {
            'token': jwt.encode(payload, "SECRET_KEY"),
            'refresh': jwt.encode(refresh_payload, "REFRESH_KEY"),
            'is_admin': user.is_superuser,
            'user_id': user.id
        }
        return Response(response, status=status.HTTP_200_OK)