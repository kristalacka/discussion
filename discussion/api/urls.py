from django.urls import path, include
from rest_framework_nested import routers
from api import views

router = routers.SimpleRouter()
router.register(r'users', views.UserViewSet)

router.register(r'subs', views.SubViewSet)

posts_router = routers.NestedSimpleRouter(router, r'subs', lookup='sub')
posts_router.register(r'posts', views.PostViewSet)

comments_router = routers.NestedSimpleRouter(posts_router,
                                             r'posts',
                                             lookup='post')
comments_router.register(r'comments', views.CommentViewSet)

urlpatterns = [
    path(r'', include(router.urls)),
    path(r'', include(posts_router.urls)),
    path(r'', include(comments_router.urls)),
    path('token/', views.login),
    path('token/refresh/', views.refresh),
    path('token/logout/', views.logout),
    path('register/', views.CreateUserView.as_view()),
    path('posts/', views.PostViewSet.as_view({'get': 'list'}))
]
