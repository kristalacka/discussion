from rest_framework import serializers
from rest_framework.fields import SerializerMethodField
from api.models import Sub


class SubSerializer(serializers.ModelSerializer):
    members = SerializerMethodField()
    subscribed = serializers.SerializerMethodField('get_subscribed_status')

    class Meta:
        model = Sub
        fields = ('creator', 'title', 'description', 'date_created', 'members',
                  'subscribed')
        read_only_fields = ('creator', 'date_created', 'subscribed')
        lookup_field = 'title'
        extra_kwargs = {'url': {'lookup_field': 'title'}}

    def get_members(self, obj):
        return obj.members.count()

    def create(self, validated_data):
        user = self.context['request'].user
        sub = Sub.objects.create(**validated_data, creator=user)
        return sub

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.description = validated_data.get('description',
                                                  instance.description)
        instance.save()
        return instance

    def get_subscribed_status(self, obj):
        request = self.context.get('request')
        if request.user.is_anonymous:
            return False

        return obj.members.filter(id=request.user.id).exists()
