from django.db.models.aggregates import Sum
from api.models import Post, Sub
from django.utils import timezone
from rest_framework import serializers

from api.models.votes import PostVote


class PostSerializer(serializers.ModelSerializer):
    score = serializers.SerializerMethodField()
    vote = serializers.SerializerMethodField()
    sub_title = serializers.SerializerMethodField()
    creator_username = serializers.CharField(source='creator.username',
                                             read_only=True)

    class Meta:
        model = Post
        fields = ('id', 'creator', 'title', 'text', 'date_created',
                  'last_edit', 'score', 'vote', 'sub_title',
                  'creator_username')
        read_only_fields = ('id', 'creator', 'date_created', 'last_edit',
                            'sub_title', 'creator_username', 'score', 'vote')

    def get_sub_title(self, obj):
        return obj.sub.title

    def get_score(self, obj):
        result = PostVote.objects.filter(post=obj).aggregate(Sum('vote'))
        if result and result['vote__sum']:
            return result['vote__sum']
        return 0

    def get_vote(self, obj):
        user = self.context['request'].user
        if user.is_anonymous:
            return None

        if len(vote_set := PostVote.objects.filter(post=obj, user=user)) > 0:
            return vote_set[0].vote
        return 0

    def create(self, validated_data):
        user = self.context['request'].user
        sub = Sub.objects.get(title=self.context['view'].kwargs['sub_title'])
        post = Post.objects.create(**validated_data, creator=user, sub=sub)
        return post

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.text = validated_data.get('text', instance.text)
        instance.last_edit = timezone.now()
        instance.save()

        return instance
