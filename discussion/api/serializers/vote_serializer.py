from rest_framework import serializers

votes = ((-1, 'DOWNVOTE'), (0, 'NONE'), (1, 'UPVOTE'))


class VoteSerializer(serializers.Serializer):
    vote = serializers.ChoiceField(votes)
