from django.db.models.aggregates import Sum
from rest_framework import serializers
from api.models import Comment, Post
from django.utils import timezone

from api.models.votes import CommentVote


class CommentSerializer(serializers.ModelSerializer):
    children = serializers.SerializerMethodField()
    score = serializers.SerializerMethodField()
    vote = serializers.SerializerMethodField()

    class Meta:
        model = Comment
        fields = ('id', 'creator', 'children', 'text', 'date_created',
                  'last_edit', 'score', 'vote', 'parent')
        read_only_fields = ('id', 'creator', 'date_created', 'last_edit',
                            'children')

    def get_score(self, obj):
        result = CommentVote.objects.filter(comment=obj).aggregate(Sum('vote'))
        if result['vote__sum']:
            return result['vote__sum']
        return 0

    def get_vote(self, obj):
        user = self.context['request'].user
        if user.is_anonymous:
            return None

        if len(vote_set := CommentVote.objects.filter(comment=obj,
                                                      user=user)) > 0:
            return vote_set[0].vote
        return 0

    def get_children(self, obj):
        data = Comment.objects.filter(parent=obj)
        return CommentSerializer(data, many=True, context=self.context).data

    def create(self, validated_data):
        user = self.context['request'].user

        post = Post.objects.get(pk=self.context['view'].kwargs['post_pk'])
        validated_data['post'] = post
        comment = Comment.objects.create(**validated_data, creator=user)
        return comment

    def update(self, instance, validated_data):
        instance.text = validated_data.get('text', instance.text)
        instance.last_edit = timezone.now()
        instance.save()
        return instance