from rest_framework import permissions


class IsOwner(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True

    def has_object_permission(self, request, view, obj):
        # everyone can view
        if request.method in permissions.SAFE_METHODS:
            return True

        # owner can edit
        return obj.user == request.user
