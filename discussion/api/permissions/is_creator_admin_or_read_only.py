from rest_framework import permissions


class IsCreatorAdminOrReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True

        # if request.method == 'POST':
        return bool(request.user and request.user.is_authenticated)

    def has_object_permission(self, request, view, obj):
        # everyone can view
        if request.method in permissions.SAFE_METHODS:
            return True

        # owner or admin can edit
        return obj.creator == request.user or request.user.is_superuser
