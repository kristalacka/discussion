import jwt

from rest_framework.authentication import BaseAuthentication, get_authorization_header
from rest_framework import exceptions
from django.contrib.auth.models import User
from api.models.token_blacklist import TokenBlacklist


class JwtAuthentication(BaseAuthentication):
    model = None

    def get_model(self):
        return User

    def authenticate(self, request):
        auth = get_authorization_header(request).split()
        if not auth or auth[0].lower() != b'bearer':
            return None

        if len(auth) == 1:
            msg = 'Invalid token header. No credentials provided.'
            raise exceptions.AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = 'Invalid token header'
            raise exceptions.AuthenticationFailed(msg)

        try:
            token = auth[1]
            if token == "null":
                msg = 'Null token not allowed'
                raise exceptions.AuthenticationFailed(msg)
        except UnicodeError:
            msg = 'Invalid token header. Token string should not contain invalid characters.'
            raise exceptions.AuthenticationFailed(msg)

        return self.authenticate_credentials(token)

    def authenticate_credentials(self, token):
        fail_msg = {'msg': 'token is invalid'}
        try:
            payload = jwt.decode(token, "SECRET_KEY", algorithms=["HS256"])
        except jwt.DecodeError:
            raise exceptions.AuthenticationFailed(fail_msg)
        except jwt.ExpiredSignatureError:
            raise exceptions.AuthenticationFailed({'msg': 'token has expired'})

        username = payload['username']
        userid = payload['id']
        try:
            user = User.objects.get(username=username,
                                    id=userid,
                                    is_active=True)
        except jwt.ExpiredSignatureError or jwt.DecodeError or jwt.InvalidTokenError:
            raise exceptions.AuthenticationFailed(fail_msg)
        except User.DoesNotExist:
            raise exceptions.AuthenticationFailed(fail_msg)

        if TokenBlacklist.objects.filter(token=token).exists():
            raise exceptions.AuthenticationFailed(
                {'msg': 'token invalidated by logout'})

        return (user, token)
