from rest_framework import permissions


class IsSelf(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True

        return bool(request.user and request.user.is_authenticated)

    def has_object_permission(self, request, view, obj):
        # everyone can view
        if request.method in permissions.SAFE_METHODS:
            return True

        # owner can edit
        return obj == request.user
